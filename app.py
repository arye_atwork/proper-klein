# -*- coding: utf-8 -*-
import json
from datetime import datetime

from klein import Klein
from twisted.internet import defer, reactor


app = Klein()


def setHeader(request, content_type):

    def _setHeader(previous_result, header, value):
        request.setHeader(header, value)

    d = defer.Deferred()
    d.addCallback(_setHeader, b'Access-Control-Allow-Origin', b'*')
    d.addCallback(_setHeader, b'Access-Control-Allow-Methods', b'GET')
    d.addCallback(_setHeader, b'Access-Control-Allow-Headers', b'x-prototype-version,x-requested-with')
    d.addCallback(_setHeader, b'Access-Control-Max-Age', b'2520')
    d.addCallback(_setHeader, b'Content-type', content_type)
    return d


@defer.inlineCallbacks
def cleanParams(params):
    for key in sorted(params):
        param = params[key]
        params[key] = yield param[0]

    return str(params)


@app.route('/delete/', methods=["GET"])
def delete(request):
    asyncClean = cleanParams(request.args)
    asyncClean.addCallback(request.write)       # write the result from cleanParams() to the response

    asyncSetHeader = setHeader(request, b'application/json')
    reactor.callLater(5, asyncSetHeader.callback, None)

    def render(results, req):
        req.write(json.dumps([str(datetime.now())]))

    finalResults = defer.gatherResults([asyncClean, asyncSetHeader])
    finalResults.addCallback(render, request)
    return finalResults


if __name__ == "__main__":
    app.run(host='0.0.0.0', port=12030)
